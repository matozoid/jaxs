package com.laamella.snelxml.generator;

import java.io.File;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.laamella.gencodegen.core.io.FileOutputStreamFactory;
import org.apache.xerces.xs.XSModel;
import org.junit.Test;

import x.Root;

import com.laamella.snelxml.generator.xsdreader.Schema;
import com.laamella.snelxml.generator.xsdreader.XsdReader;

public class SampleXmlGeneratorTest {
	@Test
	public void generateSampleXml() throws Exception {
		XsdReader xsdReader = new XsdReader();
		String serviceName = "ship-order";
		XSModel xsModel = xsdReader.createSchemaLoader().loadURI(new File(Root.testResourcesDirectory(), serviceName + ".xsd").toURI().toString());
		final Schema someXsd = new XsdReader().readXsd(serviceName, xsModel);

		final SampleXmlGenerator sampleXmlGenerator = new SampleXmlGenerator(new FileOutputStreamFactory(new File("target/"))){
			@Override
			protected void tweakStreamWriter(XMLStreamWriter xmlStreamWriter) throws XMLStreamException {
				xmlStreamWriter.setPrefix("ex", "http://www.w3schools.com/schema/schema_example");
			}
		};

		sampleXmlGenerator.writeSample(someXsd);
    }
}