package com.laamella.schema;

import org.jetbrains.annotations.NotNull;

public class OpenContent {
    enum Mode {INTERLEAVE, SUFFIX}

    @NotNull
    Mode mode;
    @NotNull
    Wildcard wildcard;
}
