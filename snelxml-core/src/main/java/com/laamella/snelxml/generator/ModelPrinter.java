package com.laamella.snelxml.generator;

import com.laamella.gencodegen.core.Block;
import com.laamella.snelxml.generator.xsdreader.Node;
import com.laamella.snelxml.generator.xsdreader.Schema;

public class ModelPrinter {
    private final Block block;
    private int indent;

	public ModelPrinter(final Block block) {
        this.block = block;
	}

	public void print(final Schema file) {
		block.add("[%s]", file.name).in();
		for (final Node node : file.topElements) {
			print(node);
		}
	}

	public void printAttribute(final Node attribute) {
		block.add("= %s", attribute.name.localPart);
	}

	public void print(final Node element) {
		if (element.isComplex()) {
			block.add("+ %s", element.name.localPart).in();
			for (final Node attribute : element.attributes) {
				printAttribute(attribute);
			}
			for (final Node subElement : element.children) {
				print(subElement);
			}
            block.out();
		} else {
			block.add(element.name.localPart);
		}
	}
}
