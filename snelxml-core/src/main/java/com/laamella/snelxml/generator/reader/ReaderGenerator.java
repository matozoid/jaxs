package com.laamella.snelxml.generator.reader;

import com.laamella.gencodegen.core.Block;
import com.laamella.gencodegen.core.io.OutputStreamFactory;
import com.laamella.gencodegen.java.ClassBody;
import com.laamella.gencodegen.java.JavaCodeGenerators;
import com.laamella.gencodegen.java.JavaFile;
import com.laamella.snelxml.crap.QName;
import com.laamella.snelxml.generator.CodeGenerator;
import com.laamella.snelxml.generator.model.Element;
import com.laamella.snelxml.generator.model.JavaType;
import com.laamella.snelxml.generator.model.Model;

import java.util.Map.Entry;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import static com.laamella.gencodegen.java.JavaCodeGenerators.*;
import static com.laamella.snelxml.crap.Arrays2.cons;
import static com.laamella.snelxml.crap.Strings.decapitalize;

public class ReaderGenerator extends CodeGenerator {
    public ReaderGenerator(final String packageName, OutputStreamFactory outputStreamFactory) {
        super(packageName, outputStreamFactory);
    }

    public static String name(Model model) {
        return model.baseName + "Reader";
    }

    @Override
	public void generate(Model model) throws Exception {
        final JavaFile file = new JavaFile(packageName, name(model));

        file.imports.addStatic(XMLStreamConstants.class);
        file.imports.add(javax.xml.namespace.QName.class);
        file.imports.add(XMLStreamException.class);
        file.imports.add(XMLStreamReader.class);
        final ClassBody clas = file.class_("public abstract class %s", file.name);

        for (Entry<String, String> ns : model.namespaceJavaNames.entrySet()) {
            clas.fields.add("public final static String %s=\"%s\";", ns.getValue(), ns.getKey());
        }
        for (Entry<QName, String> qName : model.qNameJavaNames.entrySet()) {
            String ns = model.namespaceJavaNames.get(qName.getKey().namespace);
            clas.fields.add("public final static QName %s=new QName(%s, \"%s\");", qName.getValue(), ns, qName.getKey().localPart);
        }

        for (final Element element : model.topLevelElements) {
            createReaderForDocument(element, clas);
        }

        for (JavaType type : model.javaTypes) {
            createTypeConverter(type, clas);
        }

        file.write(outputStreamFactory);
    }

    private void createReaderForDocument(final Element element, final ClassBody clas) {
        clas.method("public Object %s(final XMLStreamReader xmlr) throws XMLStreamException", element.readDocumentMethodName)
                .add("%s();", element.openDocumentMethodName)
                .open("do")
                .add("final int eventType = xmlr.next();")
                .open("switch (eventType)")
                .add("case START_ELEMENT:")
                .in()
                .open("if (xmlr.getName().equals(%s))", element.qNameField)
                .add("%s(xmlr);", element.readTagMethodName)
                .close("else")
                .in(fail("Unexpected child: %s", "xmlr.getName()"))
                .out("break;")
                .out("case END_ELEMENT:")
                .in(fail("Unexpected end tag: %s", "xmlr.getName()"))
                .out("case END_DOCUMENT:")
                .in("%s();", element.closeDocumentMethodName)
                .add("xmlr.close();")
                .add("return null;")
                .out("case CHARACTERS:")
                .add("break;")
                .close()
                .close("while (xmlr.hasNext());")
                .add(fail("Unclosed element"));

        clas.method("protected void %s()", element.openDocumentMethodName);
        clas.method("protected void %s()", element.closeDocumentMethodName);

        createReaderFor(element, clas);
    }

    private void createReaderForSimpleElement(final Element element, final ClassBody clas) {
        final Block reader = clas.method("public void %s(final XMLStreamReader xmlr) throws XMLStreamException", element.readTagMethodName);
        reader
                .open("do")
                .add("final int eventType = xmlr.next();")
                .open("switch (eventType)")
                .add("case START_ELEMENT:")
                .in(fail("Unexpected child: %s", "xmlr.getName()"))
                .out("case END_ELEMENT:")
                .in("return;")
                .out("case END_DOCUMENT:")
                .in(fail("Unexpected end of document"))
                .out("case CHARACTERS:")

                .in();
        createSimpleTypeHandlerCall(element, "xmlr.getText()", reader);

        reader
                .out("break;")
                .close()
                .close("while (xmlr.hasNext());")
                .add(fail("Unclosed element"));

        createSimpleTypeHandler(element, clas);
    }

    private void createSimpleTypeHandlerCall(Element element, String input, final Block readerBlock) {
        readerBlock.add("%s(%s(%s));", element.valueHandlerMethodName, element.conversionMethodName, input);
    }

    private void createSimpleTypeHandler(Element element, final ClassBody clas) {
        clas
                .method("protected void %s(final %s %s)", decapitalize(element.fullyQualifiedName), element.javaType.name, element.shortName);

        clas
                .method("protected String %s(final %s %s)", element.conversionMethodName, element.javaType.name, element.shortName)
                .add("return %s(%s);", element.javaType.methodName(), element.shortName);
    }

    private void createReaderForComplexElement(final Element element, final ClassBody clas) {
        final Block reader = clas.method("public void %s(final XMLStreamReader xmlr) throws XMLStreamException", element.readTagMethodName);
        reader.add("%s();", element.openTagMethodName);

        if (element.attributes.isEmpty()) {
            reader.open("if(xmlr.getAttributeCount()>0)");
            reader.add(fail("Unexpected attribute: %s", "xmlr.getAttributeName(0)"));
            reader.close();
        } else {
            reader
                    .open("for (int i = 0; i < xmlr.getAttributeCount(); i++)")
                    .add("final QName name = xmlr.getAttributeName(i);");
            for (Element attribute : element.attributes) {
                reader.add("if (name.equals(%s)) {", attribute.qNameField).in();
                createSimpleTypeHandlerCall(attribute, "xmlr.getAttributeValue(i)", reader);
                reader.close("else {");
            }
            reader.in(fail("Unexpected attribute: %s", "xmlr.getAttributeName(i)"));
            reader.close().close();
        }
        reader
                .open("do")
                .add("final int eventType = xmlr.next();")
                .add("switch (eventType) {")
                .in("case START_ELEMENT:")
                .in();

        if (element.children.isEmpty()) {
            reader.add(fail("Unexpected child: %s", "xmlr.getName()"));
        } else {
            for (Element child : element.children) {
                if (child.complex) {
                    reader
                            .open("if (xmlr.getName().equals(%s))", child.qNameField)
                            .add("%s(xmlr);", child.readTagMethodName)
                            .close("else");
                } else {
                    reader
                            .open("if (xmlr.getName().equals(%s))", child.qNameField)
                            .add("%s(xmlr);", child.readTagMethodName)
                            .close("else");
                }
            }
            reader.in(fail("Unexpected child: %s", "xmlr.getName()"));
            reader.out("break;");
        }

        reader
                .out("case END_ELEMENT:")
                .in("%s();", element.closeTagMethodName)
                .add("return;")
                .out("case END_DOCUMENT:")
                .in(fail("Unexpected end of document"))
                .out("case CHARACTERS:")
                .add("break;")
                .close()
                .close("while (xmlr.hasNext());")
                .add(fail("Unclosed element"));

        clas.method("protected void %s()", element.openTagMethodName);
        clas.method("protected void %s()", element.closeTagMethodName);

        for (Element attribute : element.attributes) {
            createSimpleTypeHandler(attribute, clas);
        }

        for (final Element node : element.children) {
            createReaderFor(node, clas);
        }
    }


    public static String fail(String format, Object... args) {
        return thro(AssertionError.class,
                call("String.format", cons("\"" + format + "\"", args))) + ";";
    }

    private void createReaderFor(final Element element, final ClassBody clas) {
        if (element.complex) {
            createReaderForComplexElement(element, clas);
        } else {
            createReaderForSimpleElement(element, clas);
        }
    }

    private void createTypeConverter(JavaType javaType, final ClassBody clas) {
        final Block conversionMethod = clas.method("protected String %s(final %s value)", javaType.methodName(), javaType.name);
        javaType.writeBody(clas, conversionMethod);
    }

}
