package com.laamella.schema;

import org.jetbrains.annotations.NotNull;

/**
 *
 */
public class NamespaceConstraint {
    enum Variety {ANY, ENUMERATION, NOT}
    @NotNull
    Variety variety;

}
