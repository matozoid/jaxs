package com.laamella.schema;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class AttributeDeclaration extends AnnotatedComponent {
    @NotNull
    List<Annotation> annotations;
    @NotNull
    NCName name;
    @Nullable
    AnyURI targetNamespace;
    @NotNull
    SimpleTypeDefinition simpleTypeDefinition;
    @NotNull
    Scope scope;
    @NotNull
    ValueConstraint valueConstraint;

    boolean inheritable;
}
