package com.laamella.snelxml.generator.writer;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

public class HandWrittenWriter {
	public final static String httpWwwW3schoolsComSchemaSchema_example = "http://www.w3schools.com/schema/schema_example";
	public final static String httpWwwW3schoolsComSchemaSchema_examplePrefix = "ex";

	public final static QName httpWwwW3schoolsComSchemaSchema_example_quantity = new QName(httpWwwW3schoolsComSchemaSchema_example, "quantity", httpWwwW3schoolsComSchemaSchema_examplePrefix);
	public final static QName httpWwwW3schoolsComSchemaSchema_example_shipTo = new QName(httpWwwW3schoolsComSchemaSchema_example, "shipTo", httpWwwW3schoolsComSchemaSchema_examplePrefix);
	public final static QName httpWwwW3schoolsComSchemaSchema_example_note = new QName(httpWwwW3schoolsComSchemaSchema_example, "note", httpWwwW3schoolsComSchemaSchema_examplePrefix);
	public final static QName httpWwwW3schoolsComSchemaSchema_example_title = new QName(httpWwwW3schoolsComSchemaSchema_example, "title", httpWwwW3schoolsComSchemaSchema_examplePrefix);
	public final static QName httpWwwW3schoolsComSchemaSchema_example_price = new QName(httpWwwW3schoolsComSchemaSchema_example, "price", httpWwwW3schoolsComSchemaSchema_examplePrefix);
	public final static QName httpWwwW3schoolsComSchemaSchema_example_name = new QName(httpWwwW3schoolsComSchemaSchema_example, "name", httpWwwW3schoolsComSchemaSchema_examplePrefix);
	public final static QName httpWwwW3schoolsComSchemaSchema_example_item = new QName(httpWwwW3schoolsComSchemaSchema_example, "item", httpWwwW3schoolsComSchemaSchema_examplePrefix);
	public final static QName httpWwwW3schoolsComSchemaSchema_example_address = new QName(httpWwwW3schoolsComSchemaSchema_example, "address", httpWwwW3schoolsComSchemaSchema_examplePrefix);
	public final static QName httpWwwW3schoolsComSchemaSchema_example_orderPerson = new QName(httpWwwW3schoolsComSchemaSchema_example, "orderPerson", httpWwwW3schoolsComSchemaSchema_examplePrefix);
	public final static QName httpWwwW3schoolsComSchemaSchema_example_city = new QName(httpWwwW3schoolsComSchemaSchema_example, "city", httpWwwW3schoolsComSchemaSchema_examplePrefix);
	public final static QName httpWwwW3schoolsComSchemaSchema_example_country = new QName(httpWwwW3schoolsComSchemaSchema_example, "country", httpWwwW3schoolsComSchemaSchema_examplePrefix);
	public final static QName httpWwwW3schoolsComSchemaSchema_example_shipOrder = new QName(httpWwwW3schoolsComSchemaSchema_example, "ship-order", httpWwwW3schoolsComSchemaSchema_examplePrefix);
	public final static QName $_orderId = new QName("", "orderId", "");

	protected void writeShipOrderDocument(XMLStreamWriter writer) throws XMLStreamException {
		writer.writeStartDocument();
		writeShipOrder(writer);
		writer.writeEndDocument();
		writer.flush();
	}

	private void writeShipOrder(XMLStreamWriter writer) throws XMLStreamException {
		writeStartElement(writer, httpWwwW3schoolsComSchemaSchema_example_shipOrder);
		writer.writeNamespace(httpWwwW3schoolsComSchemaSchema_examplePrefix, httpWwwW3schoolsComSchemaSchema_example);

		writeAttribute(writer, $_orderId, orderId());

		writeOrderPerson(writer);
		writeShipTo(writer);
		writeItem(writer);

		writer.writeEndElement();
	}

	protected boolean nextItem() {
		return false;
	}

	private void writeShipTo(XMLStreamWriter writer) throws XMLStreamException {
		writeName(writer);
		writeAddress(writer);
		writeCity(writer);
		writeCountry(writer);
	}

	private void writeCountry(XMLStreamWriter writer) throws XMLStreamException {
		writeStartElement(writer, httpWwwW3schoolsComSchemaSchema_example_country);
		writer.writeCharacters(country());
		writer.writeEndElement();
	}

	protected String country() {
		return null;
	}

	private void writeCity(XMLStreamWriter writer) {
		// TODO Auto-generated method stub

	}

	private void writeAddress(XMLStreamWriter writer) {
		// TODO Auto-generated method stub

	}

	private void writeName(XMLStreamWriter writer) {
		// TODO Auto-generated method stub

	}

	private void writeItem(XMLStreamWriter writer) throws XMLStreamException {
		while (nextItem()) {
			writeStartElement(writer, httpWwwW3schoolsComSchemaSchema_example_item);
			writeTitle(writer);
			writeNote(writer);
			writeQuantity(writer);
			writePrice(writer);
			writer.writeEndElement();
		}
	}

	private void writePrice(XMLStreamWriter writer) {
		// TODO Auto-generated method stub

	}

	private void writeQuantity(XMLStreamWriter writer) {
		// TODO Auto-generated method stub

	}

	private void writeNote(XMLStreamWriter writer) throws XMLStreamException {
		String note = note();
		if (note != null) {
			writeStartElement(writer, httpWwwW3schoolsComSchemaSchema_example_note);
			writer.writeCharacters(note);
			writer.writeEndElement();
		}
	}

	protected String note() {
		return null;
	}

	private void writeTitle(XMLStreamWriter writer) {
		// TODO Auto-generated method stub

	}

	private void writeOrderPerson(XMLStreamWriter writer) throws XMLStreamException {
		writeStartElement(writer, httpWwwW3schoolsComSchemaSchema_example_orderPerson);
		writer.writeCharacters(orderPerson());
		writer.writeEndElement();
	}

	protected String orderPerson() {
		return null;
	}

	private void writeAttribute(XMLStreamWriter writer, QName qName, String value) throws XMLStreamException {
		if (value != null) {
			if (qName.getNamespaceURI().equals("")) {
				writer.writeAttribute(qName.getLocalPart(), value);
			} else {
				writer.writeAttribute(qName.getPrefix(), qName.getNamespaceURI(), qName.getLocalPart(), value);
			}
		}
	}

	protected String orderId() {
		return null;
	}

	private void writeStartElement(XMLStreamWriter writer, QName qName) throws XMLStreamException {
		if (qName.getNamespaceURI().equals("")) {
			writer.writeStartElement(qName.getLocalPart());
		} else {
			writer.writeStartElement(qName.getPrefix(), qName.getLocalPart(), qName.getNamespaceURI());
		}
	}
}
