package com.laamella.snelxml.generator.xsdreader;

import com.laamella.snelxml.crap.QName;
import org.apache.xerces.xs.XSAttributeDeclaration;
import org.apache.xerces.xs.XSAttributeUse;
import org.apache.xerces.xs.XSComplexTypeDefinition;
import org.apache.xerces.xs.XSElementDeclaration;
import org.apache.xerces.xs.XSImplementation;
import org.apache.xerces.xs.XSLoader;
import org.apache.xerces.xs.XSModel;
import org.apache.xerces.xs.XSModelGroup;
import org.apache.xerces.xs.XSNamedMap;
import org.apache.xerces.xs.XSObjectList;
import org.apache.xerces.xs.XSParticle;
import org.apache.xerces.xs.XSSimpleTypeDefinition;
import org.apache.xerces.xs.XSTerm;
import org.apache.xerces.xs.XSTypeDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.apache.xerces.xs.XSConstants.ELEMENT_DECLARATION;
import static org.apache.xerces.xs.XSConstants.MODEL_GROUP;
import static org.apache.xerces.xs.XSConstants.WILDCARD;
import static org.apache.xerces.xs.XSTypeDefinition.COMPLEX_TYPE;
import static org.apache.xerces.xs.XSTypeDefinition.SIMPLE_TYPE;

// Useful reference: http://docs.jboss.org/jbossas/javadoc/4.0.2/org/jboss/xml/binding/XercesXsMarshaller.java.html

/**
 * Takes all useful information from an XSD and stores it in a Schema object.
 */
public class XsdReader {
    private Logger log = LoggerFactory.getLogger(XsdReader.class);

    /**
     * @return an object that will load XSD's into XSModels that can be passed to readXsd.
     */
    public XSLoader createSchemaLoader() throws ClassCastException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        final DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
        final XSImplementation impl = (XSImplementation) registry.getDOMImplementation("XS-Loader");
        return impl.createXSLoader(null);
    }

    /**
     * @param serviceName TODO
     * @param model       an XSModel, probably loaded by an XSLoader created by createSchemaLoader
     * @return a snelxml schema.
     */
    public Schema readXsd(String serviceName, final XSModel model) {
        if (model == null) {
            return new Schema(serviceName, new ArrayList<Node>());
        }
        final XSNamedMap components = model.getComponents(ELEMENT_DECLARATION);

        List<Node> topLevelElements = new ArrayList<Node>();
        final Schema structureFile = new Schema(serviceName, topLevelElements);
        log.trace("Going through top level elements.");
        for (final Object component : components.values()) {
            final XSElementDeclaration element = (XSElementDeclaration) component;
            log.trace("Parsing top level element {}", element);
            final Node node = parseElementChildren(null, element);
            topLevelElements.add(node);
        }
        return structureFile;
    }

    private Node parseElementChildren(final Node parent, final XSElementDeclaration xsElement) {
        log.trace("Parsing {}", xsElement);
        final XSTypeDefinition typeDefinition = xsElement.getTypeDefinition();
        switch (typeDefinition.getTypeCategory()) {
            case SIMPLE_TYPE: {
                final XSSimpleTypeDefinition simpleTypeDefinition = (XSSimpleTypeDefinition) typeDefinition;
                return new Node(parent, new QName(xsElement.getNamespace(), xsElement.getName()), convert(simpleTypeDefinition));
            }
            case COMPLEX_TYPE: {
                final XSComplexTypeDefinition complexTypeDefinition = (XSComplexTypeDefinition) typeDefinition;
                final Node element = new Node(parent, new QName(xsElement.getNamespace(), xsElement.getName()), new HashSet<Node>(), new ArrayList<Node>());

                final XSObjectList attributeUses = complexTypeDefinition.getAttributeUses();
                for (final Object attributeObject : attributeUses) {
                    final XSAttributeUse attributeUse = (XSAttributeUse) attributeObject;
                    final XSAttributeDeclaration attributeDeclaration = attributeUse.getAttrDeclaration();

                    log.trace("Attribute {{}}{}", attributeDeclaration.getNamespace(), attributeDeclaration.getName());
                    final Node attribute = new Node(element, new QName(attributeDeclaration.getNamespace(), attributeDeclaration.getName()), convert(attributeDeclaration.getTypeDefinition()));
                    element.attributes.add(attribute);
                }

                final XSParticle particle = complexTypeDefinition.getParticle();
                if (particle != null) {
                    generateParticle(particle, element);
                }
                return element;
            }
        }
        throw new AssertionError();
    }

    private SimpleTypeAttributes convert(XSSimpleTypeDefinition simpleTypeDefinition) {

        return new SimpleTypeAttributes();
    }

    private void generateParticle(final XSParticle particle, final Node parentNode) {
        log.trace("Particle {}", particle);
        final XSTerm term = particle.getTerm();
        switch (term.getType()) {
            case MODEL_GROUP:
                final XSModelGroup modelGroup = (XSModelGroup) term;
                for (final Object subParticle : modelGroup.getParticles()) {
                    final XSParticle subParticle1 = (XSParticle) subParticle;
                    generateParticle(subParticle1, parentNode);
                }
                break;
            case WILDCARD:
                // marshalled = marshalWildcard((XSWildcard) term);
                break;
            case ELEMENT_DECLARATION:
                final XSElementDeclaration xsSubElement = (XSElementDeclaration) term;
                final Node subElement = parseElementChildren(parentNode, xsSubElement);
                parentNode.children.add(subElement);
        }
    }
}
