package com.laamella.snelxml.crap;

import java.io.File;

public class Strings {
    public static String capitalize(final String string) {
        if (string.length() == 0) {
            return string;
        }
        return string.substring(0, 1).toUpperCase() + string.substring(1);
    }

    public static String decapitalize(String string) {
        if (string.length() == 0) {
            return string;
        }
        return string.substring(0, 1).toLowerCase() + string.substring(1);
    }

    public static String sanitize(String string) {
        boolean upperCaseNext = false;
        boolean firstCharacter = true;
        final StringBuilder saneString = new StringBuilder();
        for (char c : string.toCharArray()) {
            final boolean legal;
            if (firstCharacter) {
                legal = Character.isJavaIdentifierStart(c);
            } else {
                legal = Character.isJavaIdentifierPart(c);
            }
            if (legal) {
                if (upperCaseNext) {
                    c = Character.toUpperCase(c);
                }
                saneString.append(c);
                upperCaseNext = false;
            } else {
                upperCaseNext = true;
                if (firstCharacter) {
                    saneString.append("_" + c);
                }
            }
            firstCharacter = false;
        }
        if (saneString.length() == 0) {
            return "$";
        }
        return saneString.toString();
    }

    /**
     * Returns the filename portion of a file specification string.
     * Matches the equally named unix command.
     *
     * @return The filename string without extension.
     */
    public static String basename(String filename) {
        return basename(filename, extension(filename));
    }

    /**
     * Returns the filename portion of a file specification string.
     * Matches the equally named unix command.
     */
    public static String basename(String filename, String suffix) {
        int i = filename.lastIndexOf(File.separator) + 1;
        int lastDot = ((suffix != null) && (suffix.length() > 0))
                ? filename.lastIndexOf(suffix) : -1;

        if (lastDot >= 0) {
            return filename.substring(i, lastDot);
        } else if (i > 0) {
            return filename.substring(i);
        } else {
            return filename; // else returns all (no path and no extension)
        }
    }

    /**
     * Returns the extension portion of a file specification string.
     * This everything after the last dot '.' in the filename (NOT including
     * the dot).
     */
    public static String extension(String filename) {
        int lastDot = filename.lastIndexOf('.');

        if (lastDot >= 0) {
            return filename.substring(lastDot + 1);
        } else {
            return "";
        }
    }

}
