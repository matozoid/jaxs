package com.laamella.schema;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ModelGroup extends Term {
    @NotNull
    List<Annotation> annotations;

    enum Compositor {all, choice, sequence}

    @NotNull
    Compositor compositor;
    @NotNull
    List<Particle> particles;
}
