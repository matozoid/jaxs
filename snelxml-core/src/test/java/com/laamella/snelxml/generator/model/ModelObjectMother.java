package com.laamella.snelxml.generator.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import com.laamella.snelxml.crap.QName;
import com.laamella.snelxml.generator.datatypes.StringType;

public class ModelObjectMother {
	public static Model someModel() {
		Element element = new Element("abc", "Abc", "abcQName", new ArrayList<Element>(), new ArrayList<Element>(), StringType.INSTANCE, false, false);
		return new Model("abc.xsd", Arrays.asList(element), new HashMap<String, String>(), new HashMap<QName, String>(), new HashSet<JavaType>());
	}
}
