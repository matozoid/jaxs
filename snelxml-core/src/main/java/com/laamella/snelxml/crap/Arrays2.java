package com.laamella.snelxml.crap;

import java.util.Arrays;

/**
 *
 */
public class Arrays2 {
    public static Object[] cons(Object[] first, Object[] rest) {
        final Object[] newArray = Arrays.copyOf(first, first.length + rest.length);
        System.arraycopy(rest, 0, newArray, first.length, rest.length);
        return newArray;
    }

    public static Object[] cons(Object first, Object[] rest) {
        return cons(new Object[]{first}, rest);
    }

}
