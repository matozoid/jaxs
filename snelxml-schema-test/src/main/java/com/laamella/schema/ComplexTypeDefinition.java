package com.laamella.schema;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Set;

public class ComplexTypeDefinition extends TypeDefinition {
    @NotNull
    List<Annotation> annotations;
    @NotNull
    NCName name;
    @Nullable
    AnyURI targetNamespace;
    @NotNull
    TypeDefinition baseType;
    @NotNull
    Set<DerivationMethod> final_;
    // Required if {name} is ·absent·, otherwise must be ·absent·.
    // Either an Element Declaration or a Complex Type Definition.
    @Nullable
    Object context;
    @NotNull
    DerivationMethod derivationMethod;
    boolean abstract_;
    @NotNull
    Set<AttributeUse> attributeUses;
    @Nullable
    Wildcard attributeWildcard;
    @NotNull
    ContentType contentType;
    @NotNull
    Set<DerivationMethod> prohibitedSubstitutions;
    @NotNull
    List<Assertion> assertions;
}
