package com.laamella.snelxml.generator.model;

import static com.laamella.snelxml.crap.Strings.capitalize;
import static com.laamella.snelxml.crap.Strings.sanitize;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.laamella.snelxml.crap.QName;
import com.laamella.snelxml.crap.Strings;

public class Model {
	public final Map<String, String> namespaceJavaNames;
	public final Map<QName, String> qNameJavaNames;
	public final String xsdFileName;
	public final List<Element> topLevelElements;
	public final Set<JavaType> javaTypes;
	public final String baseName;

	Model(String xsdFileName, List<Element> topLevelElements, Map<String, String> namespaceJavaNames, Map<QName, String> qNameJavaNames, Set<JavaType> javaTypes) {
		this.xsdFileName = xsdFileName;
		this.topLevelElements = topLevelElements;
		this.namespaceJavaNames = namespaceJavaNames;
		this.qNameJavaNames = qNameJavaNames;
		this.javaTypes = javaTypes;

		baseName = capitalize(sanitize(Strings.basename(xsdFileName)));
	}
}
