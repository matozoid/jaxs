package com.laamella.schema;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ContentType {
    enum Variety {EMPTY, SIMPLE, ELEMENT_ONLY, MIXED}

    @NotNull
    Variety variety;
    @Nullable
    Particle particle;
    @Nullable
    OpenContent openContent;
    @NotNull
    SimpleTypeDefinition simpleTypeDefinition;
}
