package com.laamella.schema;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Set;

/**
 * http://www.w3.org/TR/xmlschema11-1/#cAttribute_Group_Definitions
 */
public class AttributeGroupDefinition extends AnnotatedComponent {
    @NotNull
    List<Annotation> annotations;
    @NotNull
    NCName name;
    @Nullable
    AnyURI targetNamespace;
    @NotNull
    Set<AttributeUse> attributeUses;
    @Nullable
    Wildcard wildcard;
}
