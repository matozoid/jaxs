package com.laamella.snelxml.crap;

import java.util.Map;

public final class Maps {
    public static interface ValueFactory<K, V> {
        V create(K k);
    }

    public static <K,V> V getOrCreate(Map<K,V> map,  final K k, ValueFactory<K, V> valueFactory) {
        V v = map.get(k);
        if (v == null) {
            v = valueFactory.create(k);
            map.put(k, v);
        }
        return v;
    }
}
