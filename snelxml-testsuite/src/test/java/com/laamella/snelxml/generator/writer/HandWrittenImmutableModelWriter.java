package com.laamella.snelxml.generator.writer;

import java.util.Arrays;
import java.util.Iterator;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.bla.bla.Item;
import com.bla.bla.ShipOrder;
import com.bla.bla.ShipTo;

public class HandWrittenImmutableModelWriter extends HandWrittenWriter {
	private ShipOrder shipOrder;
	private Iterator<Item> itemIterator;
	private Item item;
	private ShipTo shipTo;

	public void write(ShipOrder shipOrder, XMLStreamWriter writer) throws XMLStreamException {
		initializeShipOrderFields(shipOrder);
		writeShipOrderDocument(writer);
	}

	private void initializeShipOrderFields(ShipOrder shipOrder) {
		this.shipOrder = shipOrder;
		this.itemIterator = Arrays.asList(shipOrder.getItem()).iterator();
		initializeShipToFields(shipOrder.getShipTo());
	}

	private void initializeShipToFields(ShipTo shipTo) {
		this.shipTo = shipTo;
	}

	@Override
	protected String orderId() {
		return shipOrder.getOrderId();
	}

	@Override
	protected String orderPerson() {
		return shipOrder.getOrderPerson();
	}

	@Override
	protected String country() {
		return shipTo.getCountry();
	}

	@Override
	protected String note() {
		return item.getNote();
	}

	@Override
	protected boolean nextItem() {
		if (!itemIterator.hasNext()) {
			return false;
		}
		item = itemIterator.next();
		initializeItemFields();
		return true;
	}

	private void initializeItemFields() {
		
	}

}
