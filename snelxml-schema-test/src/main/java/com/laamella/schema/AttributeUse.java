package com.laamella.schema;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class AttributeUse {
    @NotNull
    List<Annotation>annotations;
    boolean required;
    @NotNull
    AttributeDeclaration attributeDeclaration;
    @Nullable ValueConstraint  valueConstraint;
    boolean inheritable;
}
