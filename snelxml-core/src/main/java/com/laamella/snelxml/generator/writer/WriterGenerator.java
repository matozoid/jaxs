package com.laamella.snelxml.generator.writer;

import com.laamella.gencodegen.core.io.OutputStreamFactory;
import com.laamella.snelxml.generator.CodeGenerator;
import com.laamella.snelxml.generator.model.Model;

/**
 * Generates a class that will serialize a data to an XML file according to an XSD.
 */
public class WriterGenerator extends CodeGenerator {

	public WriterGenerator(String packageName, OutputStreamFactory outputStreamFactory) {
		super(packageName, outputStreamFactory);
	}

	@Override
	public void generate(Model model) throws Exception {
	}
}
