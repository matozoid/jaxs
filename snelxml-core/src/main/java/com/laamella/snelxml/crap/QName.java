package com.laamella.snelxml.crap;

import static javax.xml.XMLConstants.NULL_NS_URI;

public class QName {
	public final String namespace;
	public final String localPart;

	public QName(String namespace, String localPart) {
		Assert.notNull(localPart);
		this.namespace = namespace == null ? NULL_NS_URI : namespace;
		this.localPart = localPart;
	}

	public QName(String localPart) {
		this(NULL_NS_URI, localPart);
	}

	@Override
	public final boolean equals(Object objectToTest) {
		if (objectToTest == null || !(objectToTest instanceof QName)) {
			return false;
		}

		final QName qName = (QName) objectToTest;
		return namespace.equals(qName.namespace) && localPart.equals(qName.localPart);
	}

	@Override
	public final int hashCode() {
		return namespace.hashCode() ^ localPart.hashCode();
	}

	@Override
	public String toString() {
		if (namespace.equals(NULL_NS_URI)) {
			return localPart;
		}
		return "{" + namespace + "}" + localPart;
	}
}
