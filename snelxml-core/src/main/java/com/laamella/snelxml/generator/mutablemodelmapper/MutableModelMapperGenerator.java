package com.laamella.snelxml.generator.mutablemodelmapper;

import com.laamella.gencodegen.core.io.OutputStreamFactory;
import com.laamella.gencodegen.java.ClassBody;
import com.laamella.gencodegen.java.JavaFile;
import com.laamella.snelxml.generator.CodeGenerator;
import com.laamella.snelxml.generator.gathermappertemplate.MapperTemplateGenerator;
import com.laamella.snelxml.generator.model.Model;

/**
 * Generates an immutable data model, and the mapper to populate it.
 */
public class MutableModelMapperGenerator extends CodeGenerator {
	public MutableModelMapperGenerator(final String packageName, OutputStreamFactory outputStreamFactory) {
		super(packageName, outputStreamFactory);
	}

	public static String name(Model model) {
		return model.baseName + "Mapper";
	}

	@Override
	public void generate(Model model) throws Exception {
		final JavaFile file = new JavaFile(packageName, name(model));
		final ClassBody clas = file.class_("public class %s extends %s", name(model), MapperTemplateGenerator.name(model));
		file.write(outputStreamFactory);
	}
}