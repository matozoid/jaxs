package com.laamella.snelxml.generator;

import java.io.File;
import java.util.List;

import com.laamella.gencodegen.core.io.FileOutputStreamFactory;
import org.apache.maven.model.Resource;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.apache.xerces.xs.XSLoader;
import org.apache.xerces.xs.XSModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.impl.StaticLoggerBinder;

import com.laamella.snelxml.generator.gathermappertemplate.MapperTemplateGenerator;
import com.laamella.snelxml.generator.immutablemodel.ImmutableModelGenerator;
import com.laamella.snelxml.generator.immutablemodelmapper.ImmutableModelMapperGenerator;
import com.laamella.snelxml.generator.model.Model;
import com.laamella.snelxml.generator.model.ModelTransformer;
import com.laamella.snelxml.generator.reader.ReaderGenerator;
import com.laamella.snelxml.generator.xsdreader.Schema;
import com.laamella.snelxml.generator.xsdreader.XsdReader;

@Mojo(name = "generate", defaultPhase = LifecyclePhase.GENERATE_SOURCES)
public class SnelXmlMojo extends AbstractMojo {
    private Logger log;
    @Parameter(defaultValue = "${project.build.directory}/generated-sources/snelxml", property = "sourceGenerationDirectory", required = true)
    private File sourceGenerationDirectory;

    @Parameter(defaultValue = "${basedir}/src/main/xsd", property = "schemaDirectory", required = true)
    private File schemaDirectory;

    @Parameter(property = "xsds", required = true)
    private List<String> xsds;

    @Component
    private MavenProject mavenProject;

    private final XsdReader xsdReader = new XsdReader();

    public void execute() throws MojoExecutionException {
        StaticLoggerBinder.getSingleton().setLog(getLog());
        log = LoggerFactory.getLogger(SnelXmlMojo.class);

        try {
            if (xsds.isEmpty()) {
                log.warn("No xsd's selected for generation.");
            }
            final CodeGenerator readerGenerator = new ReaderGenerator("com.bla.bla", new FileOutputStreamFactory(sourceGenerationDirectory));
            final CodeGenerator gatherMapperGenerator = new MapperTemplateGenerator("com.bla.bla", new FileOutputStreamFactory(sourceGenerationDirectory));
            final CodeGenerator immutableModelMapperGenerator = new ImmutableModelMapperGenerator("com.bla.bla", new FileOutputStreamFactory(sourceGenerationDirectory));
            final CodeGenerator immutableModelGenerator = new ImmutableModelGenerator("com.bla.bla", new FileOutputStreamFactory(sourceGenerationDirectory));
//            final CodeGenerator mutableModelMapperGenerator = new MutableModelMapperGenerator("com.bla.bla", new FileOutputStreamFactory(sourceGenerationDirectory));
            final SampleXmlGenerator sampleXmlGenerator = new SampleXmlGenerator(new FileOutputStreamFactory(sourceGenerationDirectory));
            final XSLoader schemaLoader = xsdReader.createSchemaLoader();
            final ModelTransformer modelTransformer = new ModelTransformer();

            for (String xsd : xsds) {
                log.info("Processing: {}", xsd);
                final XSModel xsModel = schemaLoader.loadURI(new File(schemaDirectory, xsd).toURI().toString());
                Schema schema = xsdReader.readXsd(xsd, xsModel);
                final Model model = modelTransformer.transform(schema);
                readerGenerator.generate(model);
                gatherMapperGenerator.generate(model);
                immutableModelGenerator.generate(model);
                immutableModelMapperGenerator.generate(model);
                sampleXmlGenerator.writeSample(schema);
            }
        } catch (Exception e) {
            throw new MojoExecutionException("Failed to run generators", e);
        }
        updateProject();
    }

    private void updateProject() {
        mavenProject.addCompileSourceRoot(sourceGenerationDirectory.getAbsolutePath());
        Resource resource = new Resource();
        resource.setDirectory(sourceGenerationDirectory.getAbsolutePath());
        resource.setFiltering(false);
        mavenProject.addResource(resource);
    }
}
