package com.laamella.schema;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 *
 */
public class Wildcard extends Term {
    @NotNull
    List<Annotation> annotations;
    @NotNull
    NamespaceConstraint namespaceConstraint;

    enum ProcessContents {SKIP, STRICT, LAX}

    @NotNull
    ProcessContents processContents;
}
