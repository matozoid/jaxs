package com.laamella.snelxml.generator.model;

import com.laamella.snelxml.crap.Maps;
import com.laamella.snelxml.crap.QName;
import com.laamella.snelxml.crap.Strings;
import com.laamella.snelxml.generator.datatypes.StringType;
import com.laamella.snelxml.generator.xsdreader.Node;
import com.laamella.snelxml.generator.xsdreader.Schema;
import com.laamella.snelxml.generator.xsdreader.SimpleTypeAttributes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.laamella.snelxml.crap.Strings.capitalize;
import static com.laamella.snelxml.crap.Strings.sanitize;

/**
 * Turns data from XSDs into data for generating code.
 */
public class ModelTransformer {
    final Map<Node, Element> nodeMetaDatas = new HashMap<Node, Element>();
    /**
     * actual ns => java identifier for ns
     */
    final Map<String, String> namespaceJavaNames = new HashMap<String, String>();
    /**
     * actual qname => java identifier for qname
     */
    final Map<QName, String> qNameJavaNames = new HashMap<QName, String>();
    final Set<String> javaNameDictionary = new HashSet<String>();
    final Set<JavaType> dataTypes = new HashSet<JavaType>();

    public Model transform(Schema schema) {
        final List<Element> topLevelElements = new ArrayList<Element>();
        for (final Node node : schema.topElements) {
            topLevelElements.add(transform(node));
        }
        // TODO generate package name
        return new Model(schema.name, topLevelElements, namespaceJavaNames, qNameJavaNames, dataTypes);
    }

    private String createJavaNameForNode(final Node node) {
        final String nodeName = Strings.decapitalize(sanitize(node.name.localPart));
        final String nodeNameCapitalized = capitalize(nodeName);

        if (!javaNameDictionary.contains(nodeNameCapitalized) && !javaNameDictionary.contains(nodeName)) {
            javaNameDictionary.add(nodeName);
            javaNameDictionary.add(nodeNameCapitalized);
            return nodeNameCapitalized;
        }

        final String prefix;
        if (node.parent != null) {
            prefix = createJavaNameForNode(node.parent);
        } else {
            prefix = "";
        }

        return prefix + nodeNameCapitalized;
    }

    private String createJavaNameForQName(QName name) {
        String ns = createJavaNameForNamespace(name.namespace);
        String qName = ns + "_" + sanitize(name.localPart);
        if (!qNameJavaNames.keySet().contains(name)) {
            qNameJavaNames.put(name, qName);
            // TODO collision detection
            javaNameDictionary.add(qName);
        }
        return qName;
    }

    private String createJavaNameForNamespace(String namespace) {
        final String ns;
        if (namespace.equals("")) {
            ns = "defaultNamespace";
        } else {
            ns = sanitize(namespace);
        }

        if (!namespaceJavaNames.keySet().contains(namespace)) {
            // TODO collision detection
            javaNameDictionary.add(ns);
            namespaceJavaNames.put(namespace, ns);
        }
        return ns;
    }

    private Element transform(Node node) {
        Element newNode = Maps.getOrCreate(nodeMetaDatas, node, new Maps.ValueFactory<Node, Element>() {
            public Element create(Node oldNode) {
                String qNameField = createJavaNameForQName(oldNode.name);
                List<Element> childNodes = new ArrayList<Element>();
                for (Node child : oldNode.children) {
                    childNodes.add(transform(child));
                }
                List<Element> attributes = new ArrayList<Element>();
                for (Node attribute : oldNode.attributes) {
                    attributes.add(transform(attribute));
                }
                // TODO sanitize localPart

                return new Element(oldNode.name.localPart, createJavaNameForNode(oldNode), qNameField, childNodes, attributes, findJavaType(oldNode.type), isOptional(oldNode.type), repeats(oldNode.type));
            }
        });
        return newNode;
    }

    private boolean repeats(SimpleTypeAttributes type) {
//        type.
        return false;
    }

    private boolean isOptional(SimpleTypeAttributes type) {
        return false;
    }

    private JavaType findJavaType(SimpleTypeAttributes xsSimpleTypeDefinition) {
        if (xsSimpleTypeDefinition == null) {
            // Complex type.
            return null;
        }
        JavaType javaType = StringType.INSTANCE;
        dataTypes.add(javaType);
        return javaType;
    }
}
