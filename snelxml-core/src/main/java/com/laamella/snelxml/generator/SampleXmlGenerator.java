package com.laamella.snelxml.generator;

import com.laamella.gencodegen.core.io.OutputStreamFactory;
import com.laamella.snelxml.crap.QName;
import com.laamella.snelxml.generator.xsdreader.Node;
import com.laamella.snelxml.generator.xsdreader.Schema;
import com.laamella.snelxml.generator.xsdreader.SimpleTypeAttributes;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import static com.laamella.snelxml.crap.Strings.sanitize;

// TODO do something about the horrible generated namespace prefixes.
public class SampleXmlGenerator {
    private final OutputStreamFactory outputStreamFactory;

    private static class Prefixer {
        private final Map<String, String> namespaceToPrefix = new HashMap<String, String>();
        private int i = 1;

        String getPrefixForNamespace(String namespace) {
            String prefix = namespaceToPrefix.get(namespace);
            if (prefix != null) {
                return prefix;
            }
            prefix = "ns" + i;
            i++;
            namespaceToPrefix.put(namespace, prefix);
            return prefix;
        }
    }

    public SampleXmlGenerator(OutputStreamFactory outputStreamFactory) {
        this.outputStreamFactory = outputStreamFactory;
    }

    public void writeSample(final Schema schema) throws Exception {
        final XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
        outputFactory.setProperty(XMLOutputFactory.IS_REPAIRING_NAMESPACES, true);
        final Prefixer prefixer = new Prefixer();

        for (final Node topElement : schema.topElements) {
            outputStreamFactory.stream("", sanitize(schema.name + "_" + topElement.name.localPart) + ".xml", outputStream -> {
				XMLStreamWriter xmlStreamWriter = outputFactory.createXMLStreamWriter(outputStream);
				tweakStreamWriter(xmlStreamWriter);
				xmlStreamWriter.writeStartDocument();
				writeChildren(xmlStreamWriter, topElement, 0, prefixer);
				xmlStreamWriter.writeEndDocument();
			});
        }
    }

    @SuppressWarnings("unused")
    protected void tweakStreamWriter(XMLStreamWriter xmlStreamWriter) throws XMLStreamException {
    }

    private void writeChildren(final XMLStreamWriter writer, final Node element, final int indentation, Prefixer prefixer) throws XMLStreamException {
        indentln(writer, indentation);
        if (element.name.namespace.equals("")) {
            writer.writeStartElement(element.name.localPart);
        } else {
            final String prefix = prefixer.getPrefixForNamespace(element.name.namespace);
            writer.writeStartElement(prefix, element.name.localPart, element.name.namespace);
        }
        if (element.isComplex()) {
            for (final Node attribute : element.attributes) {
                final QName name = attribute.name;
                if (name.namespace.equals("")) {
                    writer.writeAttribute(name.localPart, generateSampleFor(attribute.type));
                } else {
                    final String prefix = prefixer.getPrefixForNamespace(name.namespace);
                    writer.writeAttribute(prefix, name.namespace, name.localPart, generateSampleFor(attribute.type));
                }
            }
            for (Node child : element.children) {
                writeChildren(writer, child, indentation + 1, prefixer);
            }
            indentln(writer, indentation);
        } else {
            writer.writeCharacters(generateSampleFor(element.type));
        }
        writer.writeEndElement();
    }

    private String generateSampleFor(SimpleTypeAttributes type) {
        // TODO generate better samples
        return "...";
    }

    private void indentln(final XMLStreamWriter writer, final int indentation) throws XMLStreamException {
        writer.writeCharacters("\n");
        for (int i = 0; i < indentation; i++) {
            writer.writeCharacters("\t");
        }
    }
}
