package com.laamella.snelxml.generator.immutablemodel;

import com.laamella.gencodegen.core.Block;
import com.laamella.gencodegen.core.io.OutputStreamFactory;
import com.laamella.gencodegen.java.ClassBody;
import com.laamella.gencodegen.java.JavaCodeGenerators;
import com.laamella.gencodegen.java.JavaFile;
import com.laamella.snelxml.generator.CodeGenerator;
import com.laamella.snelxml.generator.model.Element;
import com.laamella.snelxml.generator.model.Model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.laamella.gencodegen.java.JavaCodeGenerators.*;

/**
 * Generates an immutable data model, and the mapper to populate it.
 */
public class ImmutableModelGenerator extends CodeGenerator {
	public ImmutableModelGenerator(String packageName, OutputStreamFactory outputStreamFactory) {
		super(packageName, outputStreamFactory);
	}

	@Override
	public void generate(Model model) throws Exception {
		Set<JavaFile> modelFiles = new HashSet<JavaFile>();
		for (Element element : model.topLevelElements) {
			createModelForElement(element, modelFiles);
		}
		for (JavaFile modelFile : modelFiles) {
			modelFile.write(outputStreamFactory);
		}
	}

	private void createModelForElement(Element element, Set<JavaFile> modelFiles) {
		if (element.complex) {
			final JavaFile modelFile = new JavaFile(packageName, element.fullyQualifiedName);
			modelFiles.add(modelFile);
			final ClassBody modelClass = modelFile.class_("public class %s", element.fullyQualifiedName);
			List<String> constructorArgs = new ArrayList<String>();

			for (Element child : element.children) {
				createModelForElement(child, modelFiles);
				createGetter(modelClass, child);
				createField(modelClass, child);
				constructorArgs.add(makeConstructorArg(child));
			}
			for (Element attribute : element.attributes) {
				createModelForElement(attribute, modelFiles);
				createGetter(modelClass, attribute);
				createField(modelClass, attribute);
				constructorArgs.add(makeConstructorArg(attribute));
			}

			final Block constructor = modelClass.method("public %s(%s)", element.fullyQualifiedName, join(", ", constructorArgs));
			for (Element child : element.children) {
				constructor.add("this.%s = %s;", child.valueHandlerMethodName, child.valueHandlerMethodName);
			}
			for (Element attribute : element.attributes) {
				constructor.add("this.%s = %s;", attribute.valueHandlerMethodName, attribute.valueHandlerMethodName);
			}
		}
	}

	private String makeConstructorArg(Element child) {
		return typeOf(child) + " " + child.valueHandlerMethodName;
	}

	private void createField(ClassBody modelClass, Element child) {
		modelClass.fields.add("private final %s %s;", typeOf(child), child.valueHandlerMethodName);
	}

	private String typeOf(Element element) {
		if (element.complex) {
			return element.fullyQualifiedName;
		}
		return element.javaType.name;
	}

	private void createGetter(ClassBody modelClass, Element child) {
		modelClass.method("public %s get%s()", typeOf(child), child.fullyQualifiedName).add("return %s;", child.valueHandlerMethodName);
	}
}