package com.laamella.schema;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 *
 */
public abstract class Particle extends Component {
    int minOccurs;
    static int UNBOUNDED = -1;
    int maxOccurs;
    @NotNull
    Term term;
    @NotNull
    List<Annotation> annotations;
}
