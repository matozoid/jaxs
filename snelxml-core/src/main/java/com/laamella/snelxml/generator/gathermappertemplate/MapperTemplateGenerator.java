package com.laamella.snelxml.generator.gathermappertemplate;

import com.laamella.gencodegen.core.Block;
import com.laamella.gencodegen.core.io.OutputStreamFactory;
import com.laamella.gencodegen.java.ClassBody;
import com.laamella.gencodegen.java.JavaFile;
import com.laamella.snelxml.generator.CodeGenerator;
import com.laamella.snelxml.generator.model.Element;
import com.laamella.snelxml.generator.model.Model;
import com.laamella.snelxml.generator.reader.ReaderGenerator;

/**
 * This mapper gathers all data it finds inside itself. The user has to grab the
 * data at the right time and populate his model. A close tag sets all nested fields back to null.
 */
public class MapperTemplateGenerator extends CodeGenerator {
    public MapperTemplateGenerator(String packageName, OutputStreamFactory outputStreamFactory) {
        super(packageName, outputStreamFactory);
    }

    public static String name(Model model) {
        return model.baseName + "MapperTemplate";
    }

    @Override
	public void generate(Model model) throws Exception {
        final JavaFile file = new JavaFile(packageName, name(model));
        final ClassBody clas = file.class_("public abstract class %s extends %s", name(model), ReaderGenerator.name(model));
        for (Element element : model.topLevelElements) {
            createMethodsForElement(element, clas);
        }
        file.write(outputStreamFactory);
    }

    private void createMethodsForElement(Element element, ClassBody clas) {
        if (element.complex) {
            final Block closeTagMethod = clas.method("public void %s()", element.closeTagMethodName);
            for (Element child : element.children) {
                createMethodsForElement(child, clas);
                if (!child.complex) {
                    closeTagMethod.add("%s = null;", child.valueHandlerMethodName);
                }
            }
            for (Element child : element.attributes) {
                createMethodsForElement(child, clas);
                closeTagMethod.add("%s = null;", child.valueHandlerMethodName);
            }
        } else {
            clas.fields.add("protected %s %s = null;", element.javaType.name, element.valueHandlerMethodName);
            final Block valueHandlerMethod = clas.method("public void %s(%s %s)", element.valueHandlerMethodName, element.javaType.name, element.shortName);
            valueHandlerMethod.add("this.%s = %s;", element.valueHandlerMethodName, element.shortName);
        }
    }
}
