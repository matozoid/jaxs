package com.laamella.snelxml.generator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.junit.Assert;
import org.junit.Test;

import x.Root;

public class SampleReaderTest {
	@Test
	public void test() throws XMLStreamException, FileNotFoundException {
		final XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		final XMLStreamReader xmlr = inputFactory.createXMLStreamReader(new FileInputStream(new File(Root.testResourcesDirectory(), "ShipOrder.xml")));

		ShipOrderTestReader reader = new ShipOrderTestReader();
		reader.readShipOrderDocument(xmlr);
		Assert.assertEquals("1244531234567890123456nameaddresscitycountrytitlenote522", reader.result.toString());
	}
}
