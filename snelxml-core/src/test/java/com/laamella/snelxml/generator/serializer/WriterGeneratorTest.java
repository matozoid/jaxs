package com.laamella.snelxml.generator.serializer;

import com.laamella.gencodegen.core.io.SystemOutputStreamFactory;
import org.junit.Test;

import com.laamella.snelxml.generator.model.ModelObjectMother;
import com.laamella.snelxml.generator.writer.WriterGenerator;

public class WriterGeneratorTest {
	@Test
	public void test() throws Exception {
		WriterGenerator generator = new WriterGenerator("x.y.z.", new SystemOutputStreamFactory());
		generator.generate(ModelObjectMother.someModel());
	}
}
