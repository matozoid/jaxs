package com.laamella.snelxml.generator.reader;

import com.laamella.gencodegen.core.io.SystemOutputStreamFactory;
import org.junit.Test;

import com.laamella.snelxml.generator.model.ModelObjectMother;

public class ReaderGeneratorTest {
	@Test
	public void test() throws Exception {
		ReaderGenerator generator = new ReaderGenerator("x.y.z.", new SystemOutputStreamFactory());
		generator.generate(ModelObjectMother.someModel());
	}
}
