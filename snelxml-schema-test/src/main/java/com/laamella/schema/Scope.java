package com.laamella.schema;

import org.jetbrains.annotations.NotNull;

public class Scope {
    enum Variety {GLOBAL, LOCAL}

    @NotNull
    Variety variety;

    // Either a Complex Type Definition or a Attribute Group Definition. Required if {variety} is local, otherwise must be ·absent·
    Object parent;
}
