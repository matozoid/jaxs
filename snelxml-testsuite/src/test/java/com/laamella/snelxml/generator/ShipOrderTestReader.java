package com.laamella.snelxml.generator;

import com.bla.bla.ShipOrderReader;

public class ShipOrderTestReader extends ShipOrderReader {
	public StringBuilder result = new StringBuilder();

	@Override
	protected void orderPerson(final String text) {
		result.append(text);
	}

	@Override
	protected void orderId(final String attributeValue) {
		result.append(attributeValue);
	}

	@Override
	protected void note(String note) {
		result.append(note);
	}

	@Override
	protected void price(String price) {
		result.append(price);
	}

	@Override
	protected void quantity(String quantity) {
		result.append(quantity);
	}

	@Override
	protected void title(String title) {
		result.append(title);
	}

	@Override
	protected void address(String address) {
		result.append(address);
	}

	@Override
	protected void city(String city) {
		result.append(city);
	}

	@Override
	protected void country(String country) {
		result.append(country);
	}

	@Override
	protected void name(String name) {
		result.append(name);
	}
}
