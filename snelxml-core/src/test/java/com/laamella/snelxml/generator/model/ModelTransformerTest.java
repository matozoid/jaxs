package com.laamella.snelxml.generator.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ModelTransformerTest {
	@Test
	public void whenNothingIsDoneThenNoNamingIsPresent() {
		ModelTransformer modelTransformer = new ModelTransformer();
		assertEquals(0, modelTransformer.namespaceJavaNames.size());
		assertEquals(0, modelTransformer.qNameJavaNames.size());
		assertEquals(0, modelTransformer.javaNameDictionary.size());
	}
}
