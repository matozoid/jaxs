package com.laamella.snelxml.generator;

import com.laamella.gencodegen.core.io.OutputStreamFactory;
import com.laamella.snelxml.generator.model.Model;

public abstract class CodeGenerator {
    protected final String packageName;
    protected final OutputStreamFactory outputStreamFactory;

    public CodeGenerator(final String packageName, OutputStreamFactory outputStreamFactory) {
        this.packageName = packageName;
        this.outputStreamFactory = outputStreamFactory;
    }

    public abstract void generate(Model model) throws Exception;
}
