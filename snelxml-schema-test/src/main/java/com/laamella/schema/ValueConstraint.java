package com.laamella.schema;

import org.jetbrains.annotations.NotNull;

public class ValueConstraint {
    enum Variety {DEFAULT, FIXED}

    @NotNull
    Variety variety;
    // TODO maybe Object?
    @NotNull
    String value;
    @NotNull
    String lexicalForm;
}

