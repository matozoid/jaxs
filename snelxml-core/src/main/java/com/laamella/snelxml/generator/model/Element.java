package com.laamella.snelxml.generator.model;

import java.util.List;

import static com.laamella.snelxml.crap.Strings.decapitalize;

public class Element {
    public final String fullyQualifiedName;
    public final String shortName;
    public final String qNameField;
    public final List<Element> children;
    public final List<Element> attributes;
    public final String closeTagMethodName;
    public final String openTagMethodName;
    public final String closeDocumentMethodName;
    public final String openDocumentMethodName;
    public final String valueHandlerMethodName;
    public final String readTagMethodName;
    public final String readDocumentMethodName;
    public final String conversionMethodName;

    public final JavaType javaType;
    public final boolean optional;
    public final boolean repeats;
    public final boolean complex;

    Element(String shortName, final String fullyQualifiedName, String qNameField, List<Element> childNodes, List<Element> attributes, JavaType javaType, boolean optional, boolean repeats) {
        this.shortName = shortName;
        this.fullyQualifiedName = fullyQualifiedName;
        this.qNameField = qNameField;
        this.children = childNodes;
        this.attributes = attributes;
        this.javaType = javaType;
        this.optional = optional;
        this.repeats = repeats;
        readTagMethodName = "read" + fullyQualifiedName;
        readDocumentMethodName = "read" + fullyQualifiedName + "Document";
        closeTagMethodName = "close" + fullyQualifiedName;
        openTagMethodName = "open" + fullyQualifiedName;
        closeDocumentMethodName = "close" + fullyQualifiedName + "Document";
        openDocumentMethodName = "open" + fullyQualifiedName + "Document";
        valueHandlerMethodName = decapitalize(fullyQualifiedName);
        conversionMethodName = "convert" + fullyQualifiedName + "ToString";
        complex = javaType == null;
    }
}
