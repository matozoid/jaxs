package x;

import java.io.File;

public class Root {
    public static String root() {
        return new File(Root.class.getResource(".").getPath(), "../../../").toString();
    }

    public static File xsdDirectory() {
        return new File(root(), "src/xsd");
    }

    public static File sourceGenerationDirectory() {
        return new File(root(), "target/generated-sources/snelxml");
    }

    public static File testResourcesDirectory() {
        return new File(root(), "src/test/resources");
    }
}
