package com.laamella.schema;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Set;

public class ElementDeclaration extends Term{
    @NotNull
    List<Annotation> annotations;
    @NotNull
    NCName name;
    @Nullable
    AnyURI targetNamespace;
    @NotNull
    TypeDefinition typeDefinition;
    @Nullable
    TypeTable typeTable;
    @NotNull
    Scope scope;
    @NotNull
    ValueConstraint valueConstraint;
    @NotNull
    Set<IdentityConstraintDefinition> identityConstraintDefinitions;
    @NotNull
    Set<ElementDeclaration> substitutionGroupAffiliations;

    enum SubstitutionGroupExclusion {EXTENSION, RESTRICTION}

    @NotNull
    Set<SubstitutionGroupExclusion> substitutionGroupExclusions;

    enum DisallowedSubtitution {SUBSTITUTION, EXTENSION, RESTRICTION}

    @NotNull
    Set<DisallowedSubtitution> disallowedSubtitutions;


    boolean nillable;
    boolean abstract_;

    static class TypeTable {
        List<TypeAlternative> alternatives;
        TypeAlternative defaultTypeDefinition;
    }
}
