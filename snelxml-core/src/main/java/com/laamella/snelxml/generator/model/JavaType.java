package com.laamella.snelxml.generator.model;

import com.laamella.gencodegen.core.Block;
import com.laamella.gencodegen.java.ClassBody;
import com.laamella.snelxml.crap.Strings;

public abstract class JavaType {
	public static final JavaType TO_STRING_CONVERTER = new JavaType("Anything") {
		@Override
		public void writeBody(ClassBody clas, Block conversionMethod) {
            conversionMethod.add("return value.toString()");
		}
	};
	public final String name;

	public JavaType(String name) {
		this.name = name;
	}

	public String methodName() {
		return "convert" + Strings.capitalize(name) + "ToString";
	}

	public abstract void writeBody(ClassBody clas, Block conversionMethod);
}
