package com.laamella.snelxml.generator.immutablemodel;

import com.laamella.gencodegen.core.io.SystemOutputStreamFactory;
import org.junit.Test;

import com.laamella.snelxml.generator.model.ModelObjectMother;

public class ImmutableModelGeneratorTest {
	@Test
	public void test() throws Exception {
		ImmutableModelGenerator generator = new ImmutableModelGenerator("x.y.z.", new SystemOutputStreamFactory());
		generator.generate(ModelObjectMother.someModel());
	}
}
