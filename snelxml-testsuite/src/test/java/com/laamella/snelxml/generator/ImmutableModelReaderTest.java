package com.laamella.snelxml.generator;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.junit.Test;

import x.Root;

import com.bla.bla.ShipOrder;
import com.bla.bla.ShipOrderMapper;

public class ImmutableModelReaderTest {
    @Test
    public void reader() throws XMLStreamException, FileNotFoundException {
        final XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        final XMLStreamReader xmlr = inputFactory.createXMLStreamReader(new FileInputStream(new File(Root.testResourcesDirectory(), "ShipOrder.xml")));

        ShipOrderMapper shipOrderMapper = new ShipOrderMapper();
        final ShipOrder shipOrder = shipOrderMapper.readShipOrderDocument(xmlr);
        assertEquals("124453", shipOrder.getOrderId());
        assertEquals("1234567890123456", shipOrder.getOrderPerson());
        assertEquals("name", shipOrder.getShipTo().getName());
        assertEquals("address", shipOrder.getShipTo().getAddress());
        assertEquals("city", shipOrder.getShipTo().getCity());
        assertEquals("country", shipOrder.getShipTo().getCountry());
        assertEquals("title", shipOrder.getItem().getTitle());
        assertEquals("note", shipOrder.getItem().getNote());
        assertEquals("5", shipOrder.getItem().getQuantity());
        assertEquals("22", shipOrder.getItem().getPrice());
    }
}
