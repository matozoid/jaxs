package com.laamella.snelxml.generator;

import java.io.File;

import com.laamella.gencodegen.core.Block;
import org.apache.xerces.xs.XSModel;
import org.junit.Test;

import x.Root;

import com.laamella.snelxml.generator.xsdreader.Schema;
import com.laamella.snelxml.generator.xsdreader.XsdReader;

public class StructurePrinterTest {
    @Test
    public void generatorTest() throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        XsdReader xsdReader = new XsdReader();
        String serviceName = "ship-order";
        XSModel xsModel = xsdReader.createSchemaLoader().loadURI(new File(Root.testResourcesDirectory(), serviceName + ".xsd").toURI().toString());
        final Schema someXsd = new XsdReader().readXsd(serviceName, xsModel);
        Block block = new Block();
        new ModelPrinter(block).print(someXsd);
        System.out.print(block);
    }
}
