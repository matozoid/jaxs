package com.laamella.snelxml.generator.immutablemodelmapper;

import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import com.laamella.gencodegen.core.Block;
import com.laamella.gencodegen.core.io.OutputStreamFactory;
import com.laamella.gencodegen.java.ClassBody;
import com.laamella.gencodegen.java.JavaCodeGenerators;
import com.laamella.gencodegen.java.JavaFile;
import com.laamella.snelxml.generator.CodeGenerator;
import com.laamella.snelxml.generator.gathermappertemplate.MapperTemplateGenerator;
import com.laamella.snelxml.generator.model.Element;
import com.laamella.snelxml.generator.model.Model;

import static com.laamella.gencodegen.core.CoreCodeGenerators.join;

/**
 * Generates a mapper that populates an immutable model.
 */
public class ImmutableModelMapperGenerator extends CodeGenerator {
    public ImmutableModelMapperGenerator(String packageName, OutputStreamFactory outputStreamFactory) {
        super(packageName, outputStreamFactory);
    }

    public static String name(Model model) {
        return model.baseName + "Mapper";
    }

    @Override
	public void generate(Model model) throws Exception {
        final JavaFile generatorFile = new JavaFile(packageName, name(model));
        generatorFile.imports.add(XMLStreamException.class);
        generatorFile.imports.add(XMLStreamReader.class);
        final ClassBody generatorClass = generatorFile.class_("public class %s extends %s", name(model), MapperTemplateGenerator.name(model));
        for (Element element : model.topLevelElements) {
            createMethodsForElement(element, generatorClass);
            createDocumentReader(element, generatorClass);
        }
        generatorFile.write(outputStreamFactory);
    }
    private void createDocumentReader(Element element, ClassBody generatorClass){
    	// FIXME hardcoded values
        generatorClass.method("public ShipOrder readShipOrderDocument(final XMLStreamReader xmlr) throws XMLStreamException")
            .add("super.readShipOrderDocument(xmlr);")
            .add("return shipOrder;");
    }

    private void createMethodsForElement(Element element, ClassBody generatorClass) {
        if (element.complex) {
            final Block closeTagMethod = generatorClass.method("public void %s()", element.closeTagMethodName);
            generatorClass.fields.add("protected %s %s = null;", element.fullyQualifiedName, element.valueHandlerMethodName);

            List<String> constructorArgs = new ArrayList<String>();

            for (Element child : element.children) {
                createMethodsForElement(child, generatorClass);
                constructorArgs.add(child.valueHandlerMethodName);
            }
            for (Element child : element.attributes) {
                constructorArgs.add(child.valueHandlerMethodName);
            }

            closeTagMethod.add("%s = new %s(%s);", element.valueHandlerMethodName, element.fullyQualifiedName, join(", ", constructorArgs));
            closeTagMethod.add("super.%s();", element.closeTagMethodName);
        }
    }
}