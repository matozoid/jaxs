package com.laamella.snelxml.generator.writer;

import com.bla.bla.Item;
import com.bla.bla.ShipOrder;
import com.bla.bla.ShipTo;

import org.junit.Test;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

public class WriterGeneratorTest {
	@Test
	public void test() throws XMLStreamException {
		final HandWrittenImmutableModelWriter modelSerializer = new HandWrittenImmutableModelWriter();
		ShipOrder shipOrder = new ShipOrder("weiro", new ShipTo("name", "address", "city", "country"), new Item("title", "note", "quantity", "price"), "1234");

		final XMLStreamWriter xmlWriter = XMLOutputFactory.newInstance().createXMLStreamWriter(System.out);
		modelSerializer.write(shipOrder, xmlWriter);
	}
}
