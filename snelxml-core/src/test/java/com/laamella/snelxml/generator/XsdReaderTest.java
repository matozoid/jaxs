package com.laamella.snelxml.generator;

import com.laamella.snelxml.generator.xsdreader.Schema;
import com.laamella.snelxml.generator.xsdreader.XsdReader;
import org.apache.xerces.xs.XSModel;
import org.junit.Test;
import x.Root;

import java.io.File;

import static org.junit.Assert.assertEquals;

public class XsdReaderTest {
	@Test
	public void generateSampleXml() throws Exception {
		XsdReader xsdReader = new XsdReader();
		String serviceName = "ship-order";
		XSModel xsModel = xsdReader.createSchemaLoader().loadURI(new File(Root.testResourcesDirectory(), serviceName + ".xsd").toURI().toString());
		final Schema someXsd = new XsdReader().readXsd(serviceName, xsModel);

        assertEquals(serviceName, someXsd.name);
    }
}