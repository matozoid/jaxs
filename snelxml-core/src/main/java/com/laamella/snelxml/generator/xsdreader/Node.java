package com.laamella.snelxml.generator.xsdreader;

import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;

import java.util.List;
import java.util.Set;

import org.apache.xerces.xs.XSSimpleTypeDefinition;

import com.laamella.snelxml.crap.QName;

public class Node {
	/** null when it's a root node */
	public final Node parent;
	public final QName name;
	public final SimpleTypeAttributes type;
	public final Set<Node> attributes;
	public final List<Node> children;

	/**
	 * Simple element constructor.
	 */
	public Node(final Node parent, final QName name, SimpleTypeAttributes simpleTypeAttributes) {
		this.parent = parent;
		this.name = name;
		this.type = simpleTypeAttributes;
		attributes = emptySet();
		children = emptyList();
	}

	/**
	 * Complex Element constructor.
	 */
	public Node(final Node parent, final QName name, final Set<Node> attributes, final List<Node> children) {
		this.parent = parent;
		this.name = name;
		this.attributes = attributes;
		this.children = children;
		type = null;
	}

	public boolean isComplex() {
		return type == null;
	}
}
