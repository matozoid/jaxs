package com.laamella.schema;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class ModelGroupDefinition extends AnnotatedComponent {
    @NotNull
    List<Annotation> annotations;
    @NotNull
    NCName name;
    @Nullable
    AnyURI targetNamespace;
    @NotNull
    ModelGroup modelGroup;
}
