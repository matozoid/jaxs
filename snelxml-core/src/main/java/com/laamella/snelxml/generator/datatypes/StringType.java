package com.laamella.snelxml.generator.datatypes;

import com.laamella.gencodegen.core.Block;
import com.laamella.gencodegen.java.ClassBody;
import com.laamella.snelxml.generator.model.JavaType;

public class StringType extends JavaType {
    public static StringType INSTANCE = new StringType();

    public StringType() {
        super("String");
    }

    @Override
    public void writeBody(ClassBody clas, Block conversionMethod) {
        conversionMethod.add("return value;");
    }

}
