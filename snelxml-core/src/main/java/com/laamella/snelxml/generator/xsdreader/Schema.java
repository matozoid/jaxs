package com.laamella.snelxml.generator.xsdreader;

import java.util.List;

public class Schema  {
	public final String name;
	public final List<Node> topElements ;

	public Schema(final String name, List<Node> topElements) {
		this.name = name;
		this.topElements = topElements;
	}
}
